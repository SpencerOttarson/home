#! /usr/bin/python

import Adafruit_DHT
import Adafruit_SSD1306
import csv
import datetime as dt
import json
import psycopg2
import requests
import time
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

disp = Adafruit_SSD1306.SSD1306_128_64(None)

URL = 'https://vz8afahj22.execute-api.us-east-2.amazonaws.com/dev/conditions'

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0

timeFont = ImageFont.load_default()
valueFont = ImageFont.truetype('/usr/share/fonts/truetype/noto/NotoMono-Regular.ttf', 26)

conn = None
try:
    conn=psycopg2.connect("dbname='homedb' user='XXXXXX' password='XXXXX'")
except:
    print "I am unable to connect to the database."

cur = conn.cursor()

def main():
    print('Starting', currentTime())
    initDisplay()
    while(True):
        time.sleep(60)
        try:
            takeReading()
        except Exception as e:
            print(e)

def takeReading():
    reading = Adafruit_DHT.read_retry(Adafruit_DHT.AM2302, '22')
    write(str(currentTime()), reading[1], reading[0])
#    post(str(currentTime()), reading[1], reading[0])
    displayReading(currentTime(), reading[1], reading[0])

def write(time, temp, humidity):
    cur.execute("INSERT INTO temperature (recorded_at, temp, humidity, location) VALUES (%s, %s, %s, %s)", (time, temp, humidity, "Living Room"))
    conn.commit()

def post(time, temp, humidity):
    conditions = {
            'temperature': temp,
            'humidity': humidity,
            'recordedAt': time,
            'room': 'Living Room'
    }

    data = { 'conditions': conditions }

    print(data)
    r = requests.post(url = URL, data = json.dumps(data))

    print(r.status_code)
    print(r.json())

def currentTime():
    return dt.datetime.now()

def displayReading(time, temp, humidity):
    # Draw a black filled box to clear the image.
    draw.rectangle((0,0,width,height), outline=0, fill=0)
    draw.text((x, top), str(time.date()),  font=timeFont, fill=255)
    draw.text((x, top+8), str(time.time()), font=timeFont, fill=255)
    draw.text((x, top+16), str(round((temp * 9.0 / 5.0 + 32),1)) + " F", font=valueFont, fill=255)
    draw.text((x, top+38), str(round(humidity,1)) + " %",  font=valueFont, fill=255)

    # Display image.
    disp.image(image)
    disp.display()

def initDisplay():
    # Initialize library.
    disp.begin()

    # Clear display.
    disp.clear()
    disp.display()

main()