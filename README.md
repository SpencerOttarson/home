Add to `/etc/rc.local`

```
su pi -c "nohup /home/pi/src/temp/record.py >> /home/pi/src/temp/output 2>&1 &"
```